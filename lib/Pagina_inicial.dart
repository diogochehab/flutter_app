import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class Pagina1 extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return Pagina1State();
  }
}


class Pagina1State extends State<Pagina1> {
  int _indice = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        onTap:(int indice) {
          setState(() {
            _indice = indice;
          });
          if(_indice == 0){
            Navigator.of(context).pushNamed('/third');}
          else if(_indice == 1)
          {Navigator.of(context).pushNamed('/fourth');}
        },
          currentIndex:_indice,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.accessibility_new_rounded),
              title: Text('Tarefas'),
              activeIcon: Icon(Icons.accessibility_new_rounded,
                  color: Colors.deepPurpleAccent)
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_circle_sharp),
              title: Text('Perfil'),
              activeIcon: Icon(Icons.account_circle_sharp,
                  color: Colors.deepPurpleAccent)
          ),
        ],
      ),
      appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.deepPurpleAccent,
          title: Text('ALLife',
          style: TextStyle(fontStyle: FontStyle.italic,
          fontSize: 20,decoration: TextDecoration.overline,
          decorationColor: Colors.black, decorationStyle: TextDecorationStyle.solid,
          foreground: Paint()
            ..style = PaintingStyle.stroke
            ..strokeWidth = 1.5
            ..color = Colors.white
          ),
        ),
        ),
          body: Center(
            child: Text('Clique no  "⏰" para adicionar uma tarefa',
            style: TextStyle(fontSize: 20)),
          ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.deepPurpleAccent,
          child:Icon(Icons.access_alarms_sharp,
             color: Colors.yellow),
         onPressed: (
             ) {
           Navigator.of(context).pushNamed('/second');
         },
        ),
    );
  }
}

