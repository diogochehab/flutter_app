import 'package:flutter/material.dart';
import 'Segunda_pagina.dart';
import 'Pagina_inicial.dart';
import 'Terceira_pagina.dart';
import 'Quarta_pagina.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ALLife',
      initialRoute: '/first',
      routes: {
        '/first': (context) => Pagina1(),
        '/second': (context) => Secondpage(),
        '/third': (context)=> Terceirapag(),
        '/fourth': (context) => Quartapag(),
      },
    );

  }
}